import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LayoutModule } from './layout/layout.module';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//import {MatSortModule} from '@angular/material/sort';
//import {MatTableModule} from '@angular/material/table';
//import {MatPaginatorModule} from '@angular/material/paginator';
//import {MatCheckboxModule} from '@angular/material/checkbox';
//import {MatExpansionModule} from '@angular/material/expansion';
//import {MatSelectModule} from '@angular/material/select';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,

    BrowserAnimationsModule,
    
    ReactiveFormsModule,
    FormsModule,

    //MatSortModule,
    //MatTableModule,
    //MatPaginatorModule,
    //MatCheckboxModule,
    //MatExpansionModule,
    //MatSelectModule,

    LayoutModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
